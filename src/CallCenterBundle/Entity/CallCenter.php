<?php

namespace CallCenterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CallCenter
 *
 * @ORM\Table(name="call_center")
 * @ORM\Entity(repositoryClass="CallCenterBundle\Repository\CallCenterRepository")
 */
class CallCenter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="Company", type="varchar", nullable=)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Company;

    /**
     * @var int
     *
     * @ORM\Column(name="User", type="varchar")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $User;

    /**
     * @var int
     *
     * @ORM\Column(name="DateOfContact", type="date")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $DateofContact;

    /**
     * @var int
     *
     * @ORM\Column(name="Comment", type="varchar")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param integer $company
     *
     * @return CallCenter
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return int
     */
    public function getCompany()
    {
        return $this->company;
    }
}

